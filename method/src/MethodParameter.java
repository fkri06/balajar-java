public class MethodParameter {
    public static void main(String[] args) {
        data("Fikri", "Pekalongan");
        data("Andi", "Jambi");
        data("Sandi", "Pemalang");
    }
//    method with two arguments
    static void data(String name, String address){
        System.out.println("Name: " + name);
        System.out.println("Address: " + address);
    }
}
