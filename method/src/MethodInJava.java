public class MethodInJava {
    public static void main(String[] args) {
        sayHelloToSomeone("Fikri");
        sayHelloToSomeone("Aqsha");
        sayHelloToSomeone("Zulfa");
    }
    static void sayHelloToSomeone(String name){
        System.out.println("Hello " + name);
    }
}
