public class MethodVariableArgument {
    public static void main(String[] args) {
        String[] name = {
                "Fikri", "Andi", "Akbar",
                "Cindy", "Dian", "Joko"
        };
        sayHelloToEveryoneArr(name);
        System.out.println("=============================");
        sayHelloToEveryoneVarArg(name);
        System.out.println("=============================");
        sayHelloToEveryoneVarArg("Dewi", "Sarah", "Lukman", "Aqsha");
    }
//    using array
    static void sayHelloToEveryoneArr(String[] names){
        for (String name: names) {
            System.out.println("Hello " + name);
        }
    }

//    using variable argument
    static void sayHelloToEveryoneVarArg(String... names){
        for (String name: names) {
            System.out.println("Hello " + name);
        }
    }
}
