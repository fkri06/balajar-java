// Define multiple method with
// the same name but must have
// different arguments(numbers or type)
public class MethodOverloading {
    public static void main(String[] args) {
        System.out.println(add(2));
        System.out.println(add(2, 3));
    }
//    increment a number
    static int add(int num){
        return num++;
    }

//    add two numbers
    static int add(int num1, int num2){
        return num1 + num2;
    }
}
