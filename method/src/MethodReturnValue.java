public class MethodReturnValue {
    public static void main(String[] args) {
        int a = 10;
        int b = 2;
        int c = pow(a, b);
        System.out.println(c);
        System.out.println(pow(2, 5));
    }

//    method with int as a return value
    static int pow(int base, int exp){
        int res = 1;
        for (int i = 1; i <= exp; i++) {
            res *= base;
        }
        return res;
    }
}
