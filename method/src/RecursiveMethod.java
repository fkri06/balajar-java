public class RecursiveMethod {
    public static void main(String[] args) {
        System.out.println("adding numbers(iteration): " + addIteration(5));
        System.out.println("adding numbers(recursive): " + addRecursively(5));
    }

//    loop solution
    static int addIteration(int val){
        int res = 0;
        for (int i = val; i >=0 ; i--) {
            res += i;
        }
        return res;
    }
//    adding val till 0
    static int addRecursively(int val){
//       base case to avoid stack overflow
        if (val == 0) {
            return 0;
        }
        return addRecursively(val - 1) + val;
    }
}
