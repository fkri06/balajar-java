public class While {
    public static void main(String[] args) {
        int counter = 1;
        while(counter <= 5){
            System.out.println("Perulangan " + counter);

            // if you don't specify this
            // or if you set it up wrong
            // you'll get an infinite loop
            counter++;
        }
    }
}
