// at least will execute once
public class DoWhile {
    public static void main(String[] args) {
        int count = 20;
        do {
            System.out.println("Perulangan " + count);
            count++;
        } while (count <= 5);

        System.out.println("\nNew loop");
        // now count is 21
        do{
            System.out.println("Perulangan " + count);
            count--;
        }while (count >= 0);
    }
}
