public class Break {
    public static void main(String[] args) {
        int i = 1;
        while(true){
            System.out.println("loop " + i);
            i++;

//            if the value of i is 20
//            stop the program
            if (i == 20) {
                break;
            }
        }
    }
}
