// An array traversing
public class ForEach {
    public static void main(String[] args) {
        String[] names = {
                "Muhammad", "Fikri",
                "Aqsha", "Zulfa",
                "Ismail"
        };

//        Traverse with for loop
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }
        System.out.println("\nUsing for each: ");
//        Traverse with for each
        for (String nameInNames : names) {
            System.out.println(nameInNames);
        }
    }
}
