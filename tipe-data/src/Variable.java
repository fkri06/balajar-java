public class Variable {
    public static void main(String[] args) {
        String name;
        name = "Fikri Aqsha";

        int age = 22;

        System.out.println(name);
        System.out.println(age);

        var firstName = "Fikri";
        var lastName = "Aqsha";

        System.out.println(firstName);
        System.out.println(lastName);

        // constant variable
        final String name1 = "Fikri";
        System.out.println(name1);
        /* this is an error
        * because name1 is a constant*/
        /*name1 = "we can";*/
    }
}
