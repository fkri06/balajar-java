public class ObjectRepresentationDataType {
    public static void main(String[] args) {
        Integer thisIsObjectInt = 10;
        Long thisIsObjectLong = 200L;

        System.out.println(thisIsObjectInt);
        System.out.println(thisIsObjectLong);

        /* Conversion to an object*/
        int thisIsInt = 1000000;
        Integer integerObject = thisIsInt;

        byte byteVal = integerObject.byteValue();
    }
}
