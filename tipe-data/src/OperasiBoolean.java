public class OperasiBoolean {
    public static void main(String[] args) {
        int num1 = 100;
        int num2 = 200;

        boolean trueOrFalse = num1 >= num2;
        System.out.println(trueOrFalse);

        boolean trueOrFalse1 = num1 <= num2;

        System.out.println(trueOrFalse1);

        System.out.println("false || true: " + (trueOrFalse || trueOrFalse1));
        System.out.println("false && true: " + (trueOrFalse && trueOrFalse1));
    }
}
