public class Array {
    public static void main(String[] args) {
        Integer[] intArray;
        intArray = new Integer[5];

        intArray[0] = 1;
        intArray[1] = 100;
        intArray[2] = 101;
        intArray[3] = 102;
        intArray[4] = 103;

        System.out.println(intArray[0]);
        System.out.println(intArray[1]);
        System.out.println(intArray[2]);
        System.out.println(intArray[3]);
        System.out.println(intArray[4]);

        Integer[] intArray2 = new Integer[3];

        Integer[] arrayArray = {
                1, 2, 3, 4, 5, 6
        };

        int[] arrayArrayArray = new int[]{
                6, 5, 4, 3, 2, 1
        };

        System.out.println("ArrayArray: ");
        for (int i = 0; i < arrayArray.length; i++) {
            System.out.print(arrayArray[i]);
        }

        System.out.println("\nArrayArrayArray: ");
        for (int i = 0; i < arrayArrayArray.length; i++) {
            System.out.print(arrayArrayArray[i]);
        }

        String[][] nameAndAddress = {
                {"Fikri", "Pekalongan"},
                {"Fahd", "Bekasi"},
                {"Bram", "Cilacap"}
        };


        System.out.println("\nGet The Names");
        System.out.println(nameAndAddress[0][0]);
        System.out.println(nameAndAddress[1][0]);
        System.out.println(nameAndAddress[2][0]);

        System.out.println("\nGet The Addresses");
        System.out.println(nameAndAddress[0][1]);
        System.out.println(nameAndAddress[1][1]);
        System.out.println(nameAndAddress[2][1]);
    }
}
