public class OperasiMatematika {
    public static void main(String[] args) {
        int a = 100;
        int b = 55;

        System.out.println(a + b);
        System.out.println(a - b);
        System.out.println(a * b);
        System.out.println(a / b);
//        System.out.println(100 / 0); // exception will raised because we divided 100 by 0
        System.out.println(a % b);

//        unary operator
        int id = +10123;
        int val = -3210;

        System.out.println(id);
        System.out.println(val);

        id++;
        System.out.println(id);

        val--;
        System.out.println(val);

    }
}
