public class TipeDataNumber {
    public static void main(String[] args) {
        byte thisIsByte = 100;
        short thisIsShort = 1000;
        int thisIsInt = 100000000;
        long thisIsLong = 100000000;
        long thisIsLong2 = 100000000L;

        float thisIsFloat = 10.098F;
        double thisIsDouble = 10.098;

        int decimalInt = 6;
        int hexadecimal = 0xffaa10;
        int binary = 0b100101011;

        long cost = 1_000_000_000;
    }
}
