public class NumberConversion {
    public static void main(String[] args) {
        /*this is a widening conversion*/
        byte thisIsByte = 10;
        short thisIsshort = thisIsByte;

        /*careful when manually conversing numbers*/
        int intVar = 100000;
        byte byteVar = (byte) intVar; // this is a number overflow
    }
}
