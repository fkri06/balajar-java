public class TernaryOperator {
    public static void main(String[] args) {
        int money = 100;
        System.out.println("My Money: " + money);
        String play = money <= 50 ? "You Can't Play" : "Let's Go Play Again!!";
        System.out.println(play);
    }
}
