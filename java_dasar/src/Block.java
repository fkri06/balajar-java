public class Block {
    public static void main(String[] args) {

//        an expression
        int val = 100;

//        A statement
        System.out.println("Hello");
        System.out.println("Hello");
        System.out.println("Hello");

//        A block inside a block
        {
//            A statement
            System.out.println(val);
            System.out.println(val+12);
            System.out.println(val+11);
        }

    }
}
