// introduced in java 14
public class SwitchLambda {
    public static void main(String[] args) {
        int val = 4;
        switch (val) {
            case 1 -> System.out.println("Login Page");
            case 2 -> System.out.println("Article Page");
            case 3 -> System.out.println("Logout");
            case 4, 5 ->{
                System.out.println("Done");
                System.out.println("See You Again!");
            }
            default -> System.out.println("No option of " + val);
        }
    }
}
