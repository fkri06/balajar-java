public class SwitchStatement {
    public static void main(String[] args) {
        int val = 2;
        switch (val) {
            case 1:
                System.out.println("A login Page");
                break;
            case 2:
                System.out.println("Article Page");
                break;
            case 3:
                System.out.println("Logout");
                break;
            case 4:
            case 5:
                System.out.println("Done");
                break;
            default:
                System.out.println("No option of " + val);
        }
    }
}
