// Introduced in java 14
public class YiedlKeyword {
    public static void main(String[] args) {
        int val = 1;
        String res = switch (val){
            case 1:
                yield "Login Page";
            case 2:
                yield "Article Page";
            case 3:
                yield "Logout";
            case 4, 5:
                yield "Done\nSee You Again!!";
            default:
                yield "No Option Of " + val;
        };

        System.out.println(res);
    }
}
